# FRC C++ Vagrantfile
This project uses [Vagrant](https://www.vagrantup.com/) and creates a virtual machine to compile and build WPILib C++ code for FRC. The goal is to make and deploy code without requiring Eclipse. This is not an official method of building or deploying code, so use with caution. This should work with Windows, Mac, and Linux. 

### Installation 
- Install the [latest version of Vagrant](https://www.vagrantup.com/downloads.html). For Ubuntu users, apt-get doesn't work because the version of Vagrant in the repositories is too old. 
- Clone the repository: `git clone git@github.com:stonewareslord/frc-cpp-vagrantfile.git&&cd frc-cpp-vagrantfile`
- Start the virtual machine: `vagrant up` 
  - The first time the machine is started, your computer must be connected to the internet. It will probably take a few minutes. 

### Configuration
- Set the roboRIO host in `config/host`
- Copy your code into `src/`
- Set the main file in `config/buildfile`

### Usage
Once provisioning completes, log into the machine with `vagrant ssh`. Use the following commands to build and deploy your project:
- `build` - compiles the source code in `src` to `build/FRCUserProgram`. If there are errors in the source, they will be displayed and `FRCUserProgram` will be deleted.
- `putkey` (optional) - Sends the public key to the roboRIO for passwordless deploying. 
- `deploy` - Uploads `build/FRCUserProgram` to the roboRIO. This will prompt for a password if putkey was not run (the default password is blank). 
To stop the machine, run `vagrant halt` from the host computer. Start it back up with `vagrant up`.

### Scripting
If you would like to automate the build process, you can run the following command to build then deploy only if the build was successful:
```sh
ssh vagrant@127.0.0.1 -p 2222 -i /path/to/frc-cpp-vagrantfile/.vagrant/machines/default/virtualbox/private_key 'build&&deploy'
```
Changing /path/to/frc-cpp-vagrantfile to the path of the frc-cpp-vagrantfile directory. 
Run: `while true;do clear;build 2>&1|less;done` in the virtual machine to compile when the you press `q`

### Issues
- There is no way to automate updating wpilib
- sftp is used to deploy instead of ftp
