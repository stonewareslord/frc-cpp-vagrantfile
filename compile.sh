#!/bin/bash
cd /vagrant/src
INCLUDE=$(find /vagrant/src -type d|sed -e 's/^/-I/'|paste -sd\ )
TMP=$(find . -type d)
SRCS=$(find -type f -name \*.cpp|sed -e 's/\(.+\)\.cpp$/\1/')
cd;rm -rf ~/build/;mkdir -p ~/build/;cd ~/build
for DIR in $TMP;do
  mkdir -p $DIR
done
for SRC in $SRCS;do
  echo "Compiling $(echo -n $SRC|sed -e 's/.+\/\(.+\)\.cpp//')" to "$SRC.o"
  arm-frc-linux-gnueabi-g++ -std=c++1y -I/vagrant/wpilib/include $INCLUDE -O0 -g3 -Wall -c -fmessage-length=0 -o "$SRC.o" "/vagrant/src/$(echo $SRC|cut -b "1 2" --complement)"||exit 1
done
COMPILED=$(find -type f -name \*.cpp.o|sed -e 's/\(.+\)\.cpp\.o$/\1/')
echo Linking
arm-frc-linux-gnueabi-g++ -L/vagrant/wpilib/lib -Wl,-rpath,/opt/GenICam_v2_3/bin/Linux_armv7-a -o /vagrant/build/FRCUserProgram $COMPILED -lwpi
cd -
