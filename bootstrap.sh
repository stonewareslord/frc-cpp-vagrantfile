#!/bin/bash
# If the script was not run as root
if [ "$(id -u)" != "0" ]; then
  echo "Switching to root..."
  sudo /vagrant/bootstrap.sh 
  exit
fi
# Download wpilib from github if the folder doesn't already exist
(test -d /vagrant/wpilib||(mkdir /vagrant/wpilib;curl -sL https://github.com/stonewareslord/wpilib/tarball/master | tar -xzC /vagrant/wpilib --strip-components=1))&
# Create the build folder if it doesn't already exist
mkdir -p /vagrant/build /vagrant/src
# Install the frc toolchain if it isn't installed already
dpkg -s frc-toolchain 2>&1 >/dev/null || {
  add-apt-repository -y ppa:byteit101/frc-toolchain
  apt-get update
  apt-get install -y frc-toolchain
}
# Compiles code in the /vagrant/src folder
echo "echo 'Starting build';(test -d /vagrant/src/Debug&&test -f /vagrant/src/Debug/makefile&&(cd /vagrant/src/Debug/&&make -f makefile clean;make -f makefile;cd -))||(test -f /vagrant/build/FRCUserProgram&&rm /vagrant/build/FRCUserProgram;test -f \`cat /vagrant/config/buildfile|head -n 1\`&&(arm-frc-linux-gnueabi-g++ -std=c++1y -I/vagrant/wpilib/include '-I/vagrant/src' -O0 -g3 -Wall -c -fmessage-length=0 -o /vagrant/build/Robot.o /vagrant/src/\`cat /vagrant/config/buildfile|head -n 1\`&&arm-frc-linux-gnueabi-g++ -L/vagrant/wpilib/lib -Wl,-rpath,/opt/GenICam_v2_3/bin/Linux_armv7-a -o /vagrant/build/FRCUserProgram /vagrant/build/Robot.o -lwpi;rm /vagrant/build/Robot.o))||(/vagrant/compile.sh);echo 'Done'" > /usr/local/bin/build
# Uploads the code to the rRIO based on /vagrant/config/host file
echo "cat /vagrant/build/FRCUserProgram | ssh \`cat /vagrant/config/host|head -n 1\` 'cat > /home/lvuser/FRCUserProgram2&&rm /home/lvuser/FRCUserProgram;mv /home/lvuser/FRCUserProgram2 /home/lvuser/FRCUserProgram&&. /etc/profile.d/natinst-path.sh;chmod a+x /home/lvuser/FRCUserProgram;/usr/local/frc/bin/frcKillRobot.sh -t -r'||echo 'You probably haven\'t succesfully built yet. Run build to compile the program''" > /usr/local/bin/deploy
# Creates ssh key if it doesn't exist and cats the public key to the rRIO
echo "test -d ~/.ssh||mkdir ~/.ssh;test -f ~/.ssh/id_rsa||ssh-keygen -t rsa -f ~/.ssh/id_rsa -b 4096 -q -N '';cat ~/.ssh/id_rsa.pub|ssh \`cat /vagrant/config/host|head -n 1\` 'cat >> /tmp/key;mkdir -p ~/.ssh;cat /tmp/key >> ~/.ssh/authorized_keys;rm /tmp/key'" > /usr/local/bin/putkey
chmod +x /usr/local/bin/build /usr/local/bin/deploy /usr/local/bin/putkey
echo "Done provisioning. "
# If provisioned from Vagrant
if [ $(cd "$(dirname "$0")"; pwd) != "/vagrant" ]; then
  echo "Run 'vagrant ssh' to connect to the virtual machine"
fi
